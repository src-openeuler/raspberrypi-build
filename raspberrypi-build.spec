%global debug_package %{nil}

Name:		raspberrypi-build
Version:	2.6
Release:	11
Summary:	Scripts of building images for RaspberryPi
License:	Mulan v2
ExclusiveArch: aarch64

Provides:	raspberrypi-build = %{version}-%{release}
URL:		https://gitee.com/openeuler/raspberrypi
Source0:	https://gitee.com/openeuler/raspberrypi-build/repository/archive/v%{version}.tar.gz

Requires:	dnf-plugins-core tar parted dosfstools grep bash xz kpartx

%description
Scripts of building images for Raspberry Pi

%prep
%setup -q -c

%install
mkdir -p %{buildroot}/opt/raspberrypi-build
install -p -m 700 raspberrypi-build/*.sh %{buildroot}/opt/raspberrypi-build/
cp -a raspberrypi-build/config %{buildroot}/opt/raspberrypi-build/


%files
%defattr(-,root,root)
%dir /opt/raspberrypi-build
/opt/raspberrypi-build


%changelog
* Sun Feb 07 2021 Yafen Fang<yafen@iscas.ac.cn> - 2.6-11
- bug fix: missing files' extended attributes
- bug fix: fail to start bluetooth on boot for RPI 400

* Fri Jan 08 2021 Yafen Fang<yafen@iscas.ac.cn> - 2.5-10
- extend the root partition automatically on boot
- install openssh-clients in image

* Mon Dec 21 2020 Yafen Fang<yafen@iscas.ac.cn> - 2.3-9
- bug fix: mount and umount dir
- add License in scripts
- install and start haveged on boot

* Mon Nov 23 2020 Yafen Fang<yafen@iscas.ac.cn> - 2.2-8
- update 99-com.rules to its latest commit: a312e1166d325d98a1e237d4527b01e05b0cce3d

* Tue Oct 27 2020 Yafen Fang<yafen@iscas.ac.cn> - 2.1-7
- umount rootfs and virtual block devices of image after building script exits
- change ntp to systemd-timesyncd
- change root's password and image's name

* Fri Sep 18 2020 Yafen Fang<yafen@iscas.ac.cn> - 2.0-6
- Change source0 to url in spec.

* Fri Sep 18 2020 Yafen Fang<yafen@iscas.ac.cn> - 2.0-5
- Change root's password and image's default name.

* Wed Sep 2  2020 Yafen Fang<yafen@iscas.ac.cn> - 2.0-4
- Delete the default repo file.

* Mon Aug 31 2020 Yafen Fang<yafen@iscas.ac.cn> - 2.0-3
- Add Requires(kpartx and xz) and source url in spec.
- Mv rpmlist to config, rename installed folder.

* Wed Aug 26 2020 Yafen Fang<yafen@iscas.ac.cn> - 2.0-2
- Set _install_langs to reduce the size of the language package in the image.

* Fri Aug 21 2020 Yafen Fang<yafen@iscas.ac.cn> - 2.0-1
- Update repo to openEuler 20.09.
- Add parameters to the script(create-image.sh).

* Mon Aug 3  2020 Yafen Fang<yafen@iscas.ac.cn> - 1.0-3
- Install raspberrypi-bluetooth from config.

* Wed Jul 29 2020 Yafen Fang<yafen@iscas.ac.cn> - 1.0-2
- Install packages from config.

* Mon Jul 20 2020 Yafen Fang<yafen@iscas.ac.cn> - 1.0-1
- Init package with build scripts.